//
//  ObjectCell.swift
//  advancedtest
//
//  Created by Admin on 17.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class ObjectCell: UICollectionViewCell {
    
    let imageView: CustomImageView = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    let label: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    
    var object: Object? {
        didSet {
            label.text = object?.title
            imageView.loadImageUsingUrlString(object?.imageUrl)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        
        backgroundColor = .white
        
        addSubview(imageView)
        addSubview(label)
        addConstraintsWithFormat("H:|-8-[v0]-8-|", views: imageView)
        addConstraintsWithFormat("H:|-8-[v0]-8-|", views: label)
        addConstraintsWithFormat("V:|-8-[v0][v1(30)]-8-|", views: imageView, label)
    }
}
