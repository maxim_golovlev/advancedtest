//
//  ApiService.swift
//  advancedtest
//
//  Created by Admin on 17.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation


class ApiService: NSObject {
    
    static let sharedService = ApiService()
    
    func fetchObjects (completion: @escaping ([Object]) -> () ) {
        
        let urlString = "http://advancedtest.ru/json_for_test.php"
        
        if let url = URL(string: urlString) {
        
            URLSession.shared.dataTask(with: url, completionHandler: { (data, responce, error) in
                
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                
                let objects = self.serializeObjects(usingData: data)
                completion(objects)
                
                
            }).resume()
        }
    }
    
    func serializeObjects(usingData data: Data?) -> [Object] {
        
        var objects = [Object]()
        
        guard let data = data else { return objects }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: String]]
            
            guard let items = json else { return objects }
            
            for item in items {
                if let title =  item["title"], let imageUrl = item["image"] {
                    objects.append(Object(title: title , imageUrl: imageUrl))
                }
            }
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        return objects
    }
}
