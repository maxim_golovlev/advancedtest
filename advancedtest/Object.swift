//
//  Object.swift
//  advancedtest
//
//  Created by Admin on 17.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

struct Object {
    
    var title: String
    var imageUrl: String
}
